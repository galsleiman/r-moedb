library(corrplot)
library(ggplot2)
library(dplyr)
library(caTools)
library(rpart)
library(rpart.plot)
library(pROC)
library(tm)
library(ROSE)
library(randomForest)
library(lubridate)

#1
onionornot.raw <- read.csv('OnionOrNot.csv', stringsAsFactors = FALSE)

onion <- onionornot.raw
str(onion)
onion$label<-factor(onion$label, levels = 0:1,labels = c('no','yes'))
str(onion)

mytable <- table(onion$label)
lbls <- paste(names(mytable), "\n", mytable, sep="")
pie(mytable, labels = lbls,
    main="Relationship between articles")

table(onion$label)
ggplot(onion, aes(label)) + geom_bar()

#2


onion_corpus <- Corpus(VectorSource(onion$text))


clean.corpus <- tm_map(onion_corpus, removePunctuation)

clean.corpus <- tm_map(clean.corpus, removeNumbers)

clean.corpus <- tm_map(clean.corpus, content_transformer(tolower))

clean.corpus <- tm_map(clean.corpus, removeWords, stopwords())

clean.corpus <- tm_map(clean.corpus, stripWhitespace)

dtm <- DocumentTermMatrix(clean.corpus)
dim(dtm)


minword<-function(n){
  dtm.freq <- DocumentTermMatrix(clean.corpus, list(dictionary = findFreqTerms(dtm,n)))
  temp<-dim(dtm.freq)
  temp<-temp[2]
  return(temp)

}

v.n<-seq(100,1000,by=5)
plot(v.n,dtmbyn)

dtm.freq <- DocumentTermMatrix(clean.corpus, list(dictionary = findFreqTerms(dtm,150))) 

dim(dtm.freq)


conv_01 <- function(x){
  x <- ifelse(x>0,1,0)
  return (as.integer(x))
}

dtm.final <- apply(dtm.freq, MARGIN = 1:2, conv_01)

dtm.df <- as.data.frame(dtm.final)
conv_01_type <- function(x){
  if (x =='no') return(as.integer(0))
  return (as.integer(1))
}

onion$label <- sapply(onion$label, conv_01_type)
dtm.df$label <- onion$label
summary(dtm.df$label)
#split
filter <- sample.split(dtm.df$label,SplitRatio = 0.7 )
onion.train <- subset(dtm.df,filter==T)
onion.test <- subset(dtm.df,filter==F)

dim(dtm.df)
dim(onion.train)
dim(onion.test)

#3
#model

model <- glm(label ~., family = binomial(link = 'logit'), data = onion.train)

predition <- predict(model, onion.test, type = 'response')

hist(predition)

actual <- onion.test$label

confusion_matrix <- table(actual,predition > 0.5)
confusion_matrix 
precision <- confusion_matrix[2,2]/(confusion_matrix[2,2] + confusion_matrix[1,2])
recall <- confusion_matrix[2,2]/(confusion_matrix[2,2] + confusion_matrix[2,1]) 
total_errors <- (confusion_matrix[2,1]+confusion_matrix[1,2])/dim(onion.test)[1]
total_errors
recall 
precision


model.rt <- rpart(label~.,onion.train,method="class")
rpart.plot(model.rt, box.palette = "RdBu", shadow.col = "grey", nn=TRUE)

prp(model.rt)

predict.rt <- predict(model.rt,onion.test)

actual <- onion.test$label

conf_matrix <- table(actual,predict.rt>0.5)
recall <- conf_matrix[2,2]/(conf_matrix[2,2] + conf_matrix[2,1])
precision <- conf_matrix[2,2]/(conf_matrix[2,2] + conf_matrix[1,2])
recall
precision

roclr<- roc(onion.test$label,predition, direction = "<", levels = c(0,1))
rocrt <- roc(onion.test$label,predict.rt, direction = "<", levels = c(0,1)) 
plot(roclr, col = 'red', main = 'ROC chart')
par(new=TRUE)
plot(rocrt, col = 'blue', main = 'ROC chart')
auc(roclr)
auc(rocrt)


#4



model.rt$variable.importance

model.rt.new <- rpart(label~life+news+trump,data=onion.train,method="class")
predict.n <- predict(model.rt.new,onion.test)

actual <- onion.test$label

conf_matrix <- table(actual,predict.n>0.5)
total_errors <- (confusion_matrix[2,1]+confusion_matrix[1,2])/dim(onion.test)[1]